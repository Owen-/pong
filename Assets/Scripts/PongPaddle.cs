using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongPaddle : MonoBehaviour
{
    public float speed;
    
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetMouseButton(1))
        {
            transform.position -= Vector3.up * speed * Time.deltaTime;
        }    
    }
}
