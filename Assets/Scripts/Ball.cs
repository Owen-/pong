using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody2D rigidbody2D;
    public float force;
    void Start()
    {
        rigidbody2D.AddForce(Random.insideUnitCircle * force);
    }

}
